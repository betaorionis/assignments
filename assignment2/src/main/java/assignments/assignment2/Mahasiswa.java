package assignments.assignment2;

public class Mahasiswa implements Comparable<Mahasiswa> {
    private String npm;
    private String nama;
    private KomponenPenilaian[] komponenPenilaian;

    public Mahasiswa(String npm, String nama, KomponenPenilaian[] komponenPenilaian) {
        // buat constructor untuk Mahasiswa.
        // Note: komponenPenilaian merupakan skema penilaian yang didapat dari GlasDOS.
        this.npm = npm;
        this.nama = nama;
        this.komponenPenilaian = komponenPenilaian;
    }

    public KomponenPenilaian getKomponenPenilaian(String namaKomponen) {
        // kembalikan KomponenPenilaian yang bernama namaKomponen.
        // Note: jika tidak ada, kembalikan null atau lempar sebuah Exception.
        for (KomponenPenilaian i : komponenPenilaian) {
            if (i != null) {
                if (i.getNama().equals(namaKomponen)) {
                    return i;
                }
            } else {

            }
        }
        return null;
    }

    public String getNpm() {
        // kembalikan NPM mahasiswa.
        return this.npm;
    }

    /**
     * Mengembalikan huruf berdasarkan nilai yang diberikan.
     * @param nilaiAkhir nilai untuk dicari hurufnya.
     * @return huruf dari nilai.
     */
    private String getHuruf(double nilai) {
        return nilai >= 85 ? "A" :
            nilai >= 80 ? "A-" :
            nilai >= 75 ? "B+" :
            nilai >= 70 ? "B" :
            nilai >= 65 ? "B-" :
            nilai >= 60 ? "C+" :
            nilai >= 55 ? "C" :
            nilai >= 40 ? "D" : "E";
    }

    /**
     * Mengembalikan status kelulusan berdasarkan nilaiAkhir yang diberikan.
     * @param nilaiAkhir nilai akhir mahasiswa.
     * @return status kelulusan (LULUS/TIDAK LULUS).
     */
    private String getKelulusan(double nilaiAkhir) {
        return nilaiAkhir >= 55 ? "LULUS" : "TIDAK LULUS";
    }

    public String rekap() {
        // kembalikan rekapan sesuai dengan permintaan soal.

        String rekap = "";
        for (KomponenPenilaian i : komponenPenilaian) {
            rekap += String.format("Rerata %s: %.2f \n", i.getNama(), i.getRerata());
        }
        return rekap;
    }

    public String toString() {
        // kembalikan representasi String dari Mahasiswa sesuai permintaan soal.
        return this.npm + " - " + this.nama;
    }

    public String getDetail() {
        // kembalikan detail dari Mahasiswa sesuai permintaan soal.
        String detail = "";
        for (KomponenPenilaian i : komponenPenilaian) {
            if (i != null) {
                detail += i.getDetail() + "\n";
            }
        }

        detail += String.format("Nilai akhir: %.2f\n", this.getNilaiAkhir());
        detail += String.format("Huruf: %s\n", this.getHuruf(this.getNilaiAkhir()));
        detail += this.getKelulusan(this.getNilaiAkhir());

        return detail;
    }

    public double getNilaiAkhir() {
        // mengembalikan nilai akhir mahasiswa     
        double nilaiAkhir = 0;
        for (KomponenPenilaian i : komponenPenilaian) {
            nilaiAkhir += i.getNilai();
        }

        return nilaiAkhir;
    }
    @Override
    public int compareTo(Mahasiswa other) {
        // definisikan cara membandingkan seorang mahasiswa dengan mahasiswa lainnya.
        // Hint: bandingkan NPM-nya, String juga punya method compareTo.
        int result = this.getNpm().compareTo(other.getNpm());

        return result;
    }
}
