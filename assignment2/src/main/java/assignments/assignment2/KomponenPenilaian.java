package assignments.assignment2;

public class KomponenPenilaian {
    private String nama;
    private ButirPenilaian[] butirPenilaian;
    private int bobot;

    public KomponenPenilaian(String nama, int banyakButirPenilaian, int bobot) {
        // buat constructor untuk KomponenPenilaian.
        // Note: banyakButirPenilaian digunakan untuk menentukan panjang butirPenilaian saja
        // (tanpa membuat objek-objek ButirPenilaian-nya).
        this.nama = nama;
        this.butirPenilaian = new ButirPenilaian[banyakButirPenilaian];
        this.bobot = bobot;
    }

    /**
     * Membuat objek KomponenPenilaian baru berdasarkan bentuk KomponenPenilaian templat.
     * @param templat templat KomponenPenilaian.
     */
    private KomponenPenilaian(KomponenPenilaian templat) {
        this(templat.nama, templat.butirPenilaian.length, templat.bobot);
    }

    /**
     * Mengembalikan salinan skema penilaian berdasarkan templat yang diberikan.
     * @param templat templat skema penilaian sebagai sumber.
     * @return objek baru yang menyerupai templat.
     */
    public static KomponenPenilaian[] salinTemplat(KomponenPenilaian[] templat) {
        KomponenPenilaian[] salinan = new KomponenPenilaian[templat.length];
        for (int i = 0; i < salinan.length; i++) {
            salinan[i] = new KomponenPenilaian(templat[i]);
        }
        return salinan;
    }

    public void masukkanButirPenilaian(int idx, ButirPenilaian butir) {
        // masukkan butir ke butirPenilaian pada index ke-idx.
        butirPenilaian[idx] = butir;
    }

    public String getNama() {
        // kembalikan nama KomponenPenilaian.
        return this.nama;
    }

    public double getRerata() {
        // kembalikan rata-rata butirPenilaian.
        double total = 0;
        int nullCounter = 0;
        int sumNilai = 0;
        for (ButirPenilaian i : this.butirPenilaian) {
            if (i == null) {
                nullCounter++;
            } else {
                sumNilai += i.getNilai();
                total++;
            }
        }
        if (nullCounter == this.butirPenilaian.length) {
            return 0;
        }

        return sumNilai / total;
    }

    public double getNilai() {
        // kembalikan rerata yang sudah dikalikan dengan bobot.
        return (this.getRerata() * this.bobot / 100);
    }

    public String getDetail() {
        // kembalikan detail KomponenPenilaian sesuai permintaan soal.
        
        String detail = "~~~ " + this.nama + " (" + this.bobot + "%) ~~~\n";
        String temp = "";
        int  counter = 1;
        for (ButirPenilaian i : butirPenilaian) {
            if (i != null) {
                temp = this.nama + String.valueOf(counter) + ": " + i.getNilai();
                detail = temp + "\n";
            }
        }
        detail += String.format("Rerata: %.2f\n", this.getRerata());
        detail += String.format("Kontribusi nilai akhir: %.2f", this.getNilai());

        return detail;
    }

    @Override
    public String toString() {
        // kembalikan representasi String sebuah KomponenPenilaian sesuai permintaan soal.
        return String.format("Rerata Tugas: %.2f", this.getRerata());
    }

}
