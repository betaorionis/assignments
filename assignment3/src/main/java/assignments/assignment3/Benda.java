package assignments.assignment3;


public abstract class Benda extends Carrier{
  
    protected int persentaseMenular;

    public Benda(String name){
        super(name, "Benda");
        this.persentaseMenular = 0;
    }

    public abstract void tambahPersentase();

    public int getPersentaseMenular(){
        // Kembalikan nilai dari atribut persentaseMenular
        return this.persentaseMenular;
    }
    
    public void setPersentaseMenular(int persentase) {
        //  Gunakan sebagai setter untuk atribut persentase menular
        this.persentaseMenular = persentase;
    }
}