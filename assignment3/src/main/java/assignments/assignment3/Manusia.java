package assignments.assignment3;

public abstract class Manusia extends Carrier{
    private static int jumlahSembuh = 0;
    
    public Manusia(String nama) {
        // Buat constructor untuk Manusia.
        // Akses constructor superclass-nya
        super(nama, "Manusia");
    }
    
    public void tambahSembuh() {
        // Fungsi untuk menambahkan nilai pada atribut jumlahSembuh.
        // Perhatikan bahwa access modifiernya bertipe public
        jumlahSembuh++;
    }

    public static int getJumlahSembuh() {
        // Kembalikan nilai untuk atribut jumlahSembuh.
        return jumlahSembuh;
    }
}