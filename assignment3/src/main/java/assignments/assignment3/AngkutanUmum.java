package assignments.assignment3;

public class AngkutanUmum extends Benda{
    // Implementasikan abstract method yang terdapat pada class Benda
      
    public AngkutanUmum(String name){
        // Buat constructor untuk AngkutanUmum.
        // Akses constructor superclass-nya
        super(name);
        this.persentaseMenular = 0;
    }

    public void tambahPersentase() {
        this.persentaseMenular += 35;
    }
    
    @Override
    public String toString() {
        return "ANGKUTAN UMUM " + this.getNama();
    }
}