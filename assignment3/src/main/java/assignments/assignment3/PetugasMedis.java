package assignments.assignment3;

public class PetugasMedis extends Manusia{
  		
    private int jumlahDisembuhkan;

    public PetugasMedis(String nama){
        // Buat constructor untuk Jurnalis.
        // Hint: Akses constructor superclass-nya
        super(nama);
        this.jumlahDisembuhkan = 0;
    }

    public void obati(Manusia manusia) {
        // Implementasikan apabila objek PetugasMedis ini menyembuhkan manusia
        // Hint: Update nilai atribut jumlahDisembuhkan
        manusia.ubahStatus("negatif");
        this.jumlahDisembuhkan++;
        manusia.tambahSembuh();
    }

    public int getJumlahDisembuhkan(){
        // Kembalikan nilai dari atribut jumlahDisembuhkan
        return this.jumlahDisembuhkan;
    }

    @Override
    public String toString() {
        return "PETUGAS MEDIS " + this.getNama();
    }
}