package assignments.assignment3;

public class TombolLift extends Benda{
      // Implementasikan abstract method yang terdapat pada class Benda
      
    public TombolLift(String name){
        // Buat constructor untuk Jurnalis.
        // Hint: Akses constructor superclass-nya
        super(name);
        this.persentaseMenular = 0;
    }

    public void tambahPersentase() {
        this.persentaseMenular += 20;
    }
    
    @Override
    public String toString() {
        return "TOMBOL LIFT " + this.getNama();
    }
}