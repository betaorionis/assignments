package assignments.assignment3;

public class Positif implements Status{
  
    public String getStatus(){
        return "Positif";
    }

    public void tularkan(Carrier penular, Carrier tertular){
        // Implementasikan apabila object Penular melakukan interaksi dengan object tertular
        // Handle kasus ketika keduanya benda dapat dilakukan disini
        String tipe_penular = penular.getTipe();
        String tipe_tertular = tertular.getTipe();
        if (tipe_penular.equalsIgnoreCase("manusia") && tipe_tertular.equalsIgnoreCase("manusia")) {
            Manusia pen = (Manusia) penular;
            Manusia ter = (Manusia) tertular;

            ter.ubahStatus("positif");

        } else if (tipe_penular.equalsIgnoreCase("benda") && tipe_tertular.equalsIgnoreCase("manusia")) {
            Benda pen = (Benda) penular;
            Manusia ter = (Manusia) tertular;

            ter.ubahStatus("positif");

        } else if (tipe_penular.equalsIgnoreCase("manusia") && tipe_tertular.equalsIgnoreCase("benda")) {
            Manusia pen = (Manusia) penular;
            Benda ter = (Benda) tertular;

            ter.tambahPersentase();
            if (ter.getPersentaseMenular() >= 100) {
                ter.ubahStatus("positif");
            }
        }
    }
}